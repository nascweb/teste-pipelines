const info = require('./info');
const assert = require('assert');

let [event, date, city] = info();

assert.equal(event, 'BrasilJS Conf');
assert.equal(date, '26 e 27 de Agosto');
assert.equal(city, 'Porto Alegre');